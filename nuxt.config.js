import pkg from './package'

export default {
  mode: 'spa',

  /*
   ** Headers of the page
   */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },

  /*
   ** Show fancy loading indicator while SPA page is loading!
   */
  loadingIndicator: {
    name: 'three-bounce',
    color: '#363636',
    background: 'white'
  },

  /*
   ** Customize the generated output folder
   */
  generate: {
    dir: 'public'
  },

  /*
   ** Global CSS
   */
  css: [],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [],

  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    // Doc: https://buefy.github.io/#/documentation
    'nuxt-buefy',
    // Doc: https://nuxt-community.github.io/nuxt-i18n
    'nuxt-i18n'
  ],
  /*
   ** Axios module configuration
   */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  i18n: {
    locales: [
      {
        name: 'Català',
        code: 'ca',
        iso: 'ca-ES',
        file: 'ca.js'
      },
      {
        name: 'English',
        code: 'en',
        iso: 'en-US',
        file: 'en.js'
      }
    ],
    defaultLocale: 'en',
    lazy: true,
    langDir: 'lang/',
    vueI18n: {
      dateTimeFormats: {
        en: {
          long: {
            weekday: 'long',
            year: 'numeric',
            month: 'long',
            day: 'numeric'
          }
        },
        ca: {
          long: {
            weekday: 'long',
            year: 'numeric',
            month: 'long',
            day: 'numeric'
          }
        }
      }
    }
  },

  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
      // Load raw txt or md files
      config.module.rules.push({
        enforce: 'pre',
        test: /\.(txt|md)$/,
        loader: 'raw-loader'
      })
    }
  }
}
