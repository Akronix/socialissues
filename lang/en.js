export default {
  moto: 'Find social issues',
  submoto: 'Make communities (and the world) better through code',
  what_is_this: 'Tell me more',
  about: 'About socialissues.tech',
  view_source: 'View source',
  open_since: 'Open since',
  view_more: 'View more...',
  go_for_it: 'Go for it!',
  in: 'In',
  close: 'Close',
  source_licensed: ' The source code is licensed under',
  website_licensed: 'The website content is licensed under',
  footer_join_us: 'is a social economy ecosystem project, join us!'
}
