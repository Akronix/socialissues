#### Què

Socialissues.tech és un agregador d'issues de projectes de l'Economia Social i Solidària (ESS).
L'eina cerca issues als principals proveïdors de control de versions de codi basats en Git (fins ara Gitlab i Github) etiquetades amb `socialissues.tech`.

#### Per què

Aprofitem les comunitats potents que existeixen entorn del software lliure per enriquir l'ecosistema de l'ESS creant aquest pont entre el món més tecnològic i el món més social, fent que aquests projectes guanyin importància i visibilitat facilitant així l'entrada a noves contribuïdores.

- Per visibilitzar els projectes fora de l'ESS.
- Perquè els projectes avancin i tinguin més contribuïdores.
- Per impulsar sinergies entre els projectes.
- Per posar en valor l'organització d'un projecte FLOSS acollidor.

##### Què em pot aportar?

- Com a contribuïdora
  - T'obre portes a un conjunt d'issues amb impacte directe sobre la societat.
  - Pots aportar solucions a les diferents dificultats que poden tenir les entitats amb els teus coneixements tecnològics. Així com donar una visió tècnica global a aquests projectes.
- Com a entitat/projecte
  - Un espai on visibilitzar el vostre projecte a persones que, per altres mitjans, no arribarien a conèixer la iniciativa.
  - Solucions a les vostres issues des d'una visió externa a l'ESS, situació que no es dóna molt sovint.
  - Oportunitats per endreçar els vostres projectes i documentar-los per tal d'obrir el vostre codi a noves contribuïdores.
  - Pot suposar una empenta al vostre projecte.

#### Qui

Socialissues.tech neix de la necessitat de crear comunitats FLOSS entorn dels projectes de l'ESS.
Som una aliança entre entitats de l'ESS per ajudar-nos mútuament a evolucionar i millorar el nostre ecosistema.
El projecte és una iniciativa de [Coopdevs](https://coopdevs.org/), i hi participem un conjunt d'entitats que veiem clara aquesta necessitat i hem començat aquesta sinergia.
Totes les entitats participants aportem tot el que podem per dinamitzar aquesta nova plataforma.

Actualment, hi donem suport [Coopdevs](https://coopdevs.org/), [Adabits](https://www.adabits.org/), [Dabne](https://www.dabne.net/), [Col·lectivaT](https://collectivat.cat/) i [LliureTic](https://www.lliuretic.cat/).

Els projectes FLOSS participants actuals són [Socialissues.tech](https://socialissues.tech/), [Pam a pam](https://socialissues.tech/), [Open Food Network](https://socialissues.tech/) i [TimeOverflow](https://www.timeoverflow.org/).

Si vols participar, crea un post a https://community.coopdevs.org/tags/socialissuestech :)

#### Glossari

- _**Issue**: es tradueix com a incidència, però realment és qualsevol idea, millora, tasca o error a resoldre en el projecte._
- _**FLOSS**: Free/Libre and Open Source Software, o traduït al català, programari lliure i de codi obert._
- _**Git**: programari lliure de sistema de control de versions, permet treballar en comú en un projecte i mantenir un històric dels canvis._
- _**ESS**: Economia social i solidària: forma de produir, distribuir i consumir al servei de les persones, basada en la cooperació i el bé comú._
