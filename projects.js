export default [
  {
    provider: 'gitlab',
    name: 'coopdevs/socialissues'
  },
  {
    provider: 'github',
    name: 'openfoodfoundation/openfoodnetwork'
  },
  {
    provider: 'github',
    name: 'coopdevs/timeoverflow'
  },
  {
    provider: 'gitlab',
    name: 'pamapam/frontend'
  }
]
